# sudo apt-get install python-espeak
# pip install PyPDF4
# pip install pyttsx3

import pyttsx3
import PyPDF4
from tkinter.filedialog import *
book = askopenfilename()

pdfreader = PyPDF4.PdfFileReader(book)

pages = pdfreader.numPages

for number in range(1,pages):
	page = pdfreader.getPage(number)
	text = page.extractText()
	player = pyttsx3.init()
	player.say(text)
	player.runAndWait()